# Introducción a Python

<a href="https://faam.gitlab.io/python_intro/"><img alt="Link a la Documentación" src="https://jupyterbook.org/badge.svg"></a>
[![pipeline status](https://gitlab.com/FAAM/python_intro/badges/master/pipeline.svg)](https://gitlab.com/FAAM/python_intro/-/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/FAAM%2Fpython_intro/HEAD)

## Contenidos temáticos

* Introducción a la programación
* Introducción a Python
* Sintaxis básicas
* Flujo de Control
* Estructura de datos
* Funciones
* Errores y excepciones
* Buenas prácticas



